import React from "react";
import { useStyles } from './app/styles';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";

import {
  AppBar,
  Toolbar,
  CssBaseline,
  Typography,
} from '@material-ui/core';

import { useSelector } from 'react-redux'

import { Home as HomeIcon } from '@material-ui/icons';

import Login from "./components/Login";
import Register from "./components/Register";
import HomePage from "./components/HomePage";
import Error404 from "./components/Error404";
import About from "./components/About";
import AuthLinks from "./components/AuthLinks"

import "./App.css"

export default function App() {
  const classes = useStyles();

  return (
    <Router>
      <div>
        <AppBar position="static">
          <CssBaseline />
          <Toolbar>
            <Typography variant="h4" className={classes.logo}>
              Домашнее задание React Router & Redux
            </Typography>
            <div className={classes.navlinks}>
              <Link to="/" className={classes.link}>
                <HomeIcon></HomeIcon>
                Главная
              </Link>
              <Link to="/about" className={classes.link}>
                О пользователе
              </Link>              
              <AuthLinks></AuthLinks>
            </div>
          </Toolbar>
        </AppBar>
        <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/register">
            <Register />
          </Route>
          <PrivateRoute path="/about">
            <About />
          </PrivateRoute>
          <Route path="/">
            <HomePage />
          </Route>
          <Route path="/error404">
            <Error404 />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

function PrivateRoute({ children, ...rest }) {

  let authState = useSelector((state) => {
    return {
      isAuthorized: state.isAuthorized,
      userName: state.userName
    }
  })

  return (
    <Route
      {...rest}
      render={({ location }) =>
        authState.isAuthorized == true ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/error404",
              state: { from: location }
            }}
          />
        )
      }
    />
  );
}
